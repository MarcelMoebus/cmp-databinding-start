import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'app-server-element',
  templateUrl: './server-element.component.html',
  styleUrls: ['./server-element.component.css']
})
export class ServerElementComponent implements OnInit {
  @Input() element: {type: string, name: string, content: string};
  /* Assign an alias to bind to element through an alias (here XYZ), binding from other components nested inside this component: 
  @Input('XYZ') element: {type: string, name: string, content: string}; */
  constructor() { }

  ngOnInit(): void {
  }

}
